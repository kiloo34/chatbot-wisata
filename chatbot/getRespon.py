import re

from django.core import serializers

from chatbot.models import KategoriPesan, Respon
from chatbot.preprocessing import *
from chatbot.cosine import *

GREETING_INPUTS = ("hallo", "halo", "hai", "selamat", "pagi","siang","sore","malam","hello","helo")
PENUTUP_INPUTS = ("terima", "terimakasih", "thank")

def getData(kategori):
    # data = Respon.objects.filter('')
    data = Respon.objects.filter(kategori__nama__contains=kategori).values()
    return data

def getDataKategori():
    data = KategoriPesan.objects.all()
    return data

def message_probability(user_message, recognised_words, single_response=False, required_words=[]):
    message_certainty = 0
    has_required_words = True
    word_not_in = 0

    # Menghitung berapa banyak kata yang ada di setiap pesan yang telah ditentukan
    for word in user_message:
        if word in recognised_words:
            print('message_certainty = ',message_certainty)
            message_certainty += 1

    # Menghitung persentase kata yang dikenali dalam pesan pengguna
    percentage = float(message_certainty) / float(len(recognised_words))
    print('percentage 1 = ', percentage)

    # Memeriksa bahwa kata-kata yang diperlukan ada dalam string
    for word in required_words:
        if word not in user_message:
            print('not in')
            has_required_words = False
            break

    for word in required_words:
        if word not in user_message:
            word_not_in += 1

    if word_not_in >= 1:
        has_required_words = False
    #     break

    # Harus memiliki kata-kata yang diperlukan, atau menjadi respons tunggal
    if has_required_words or single_response:
        print('masuk')
        print('percentage 2 = ',percentage)
        return percentage * 100
    # elif word_not_in >= 1:
    #     return 0
    else:
        return 0

def TfidfCosSim(user_message, d1, d2):

    # print('\n=====================token Q==================')
    # print(user_message)
    # print('\n=====================token D1==================')
    # print(d1)
    # print('\n=====================token D2==================')
    # print(d2)

    wordSet = Cosine.Union(user_message, d1)
    wordSet = Cosine.Union(wordSet, d2)
    # print('\n=====================WordSet==================')
    # print(wordSet)

    wordDictq = Cosine.wordDict(wordSet)
    wordDictd1 = Cosine.wordDict(wordSet)
    wordDictd2 = Cosine.wordDict(wordSet)

    # print('\n=====================WordDict Q==================')
    # print(wordDictq)
    # print('\n=====================WordDict D1==================')
    # print(wordDictd1)
    # print('\n=====================WordDict D2==================')
    # print(wordDictd2)

    freqQ = Cosine.freqQ(user_message, wordDictq)
    freqD1 = Cosine.freqQ(d1, wordDictd1)
    freqD2 = Cosine.freqQ(d2, wordDictd2)

    # print('\n=====================Frequent Q==================')
    # print(freqQ)
    # print('\n=====================Frequent D1==================')
    # print(freqD1)
    # print('\n=====================Frequent D2==================')
    # print(freqD2)

    tfBowQ = Cosine.computeTFQ(wordDictq, user_message)
    tfBowD1 = Cosine.computeTFQ(wordDictd1, d1)
    tfBowD2 = Cosine.computeTFQ(wordDictd2, d2)

    # print('\n=====================TF Q==================')
    # print(tfBowQ)
    # print('\n=====================TF D1==================')
    # print(tfBowD1)
    # print('\n=====================TF D2==================')
    # print(tfBowD2)

    idfs = Cosine.computeIDF([wordDictd2,wordDictd1,wordDictq])

    # print('\n=====================IDFS==================')
    # print(idfs)

    tfidfBowQ = Cosine.computeTFIDF(tfBowQ,idfs)
    tfidfBowD1 = Cosine.computeTFIDF(tfBowD1,idfs)
    tfidfBowD2 = Cosine.computeTFIDF(tfBowD2,idfs)

    # print('\n=====================TFIDF Q==================')
    # print(tfidfBowQ)
    # print('\n=====================TFIDF D1==================')
    # print(tfidfBowD1)
    # print('\n=====================TFIDF D2==================')
    # print(tfidfBowD2)

    res1 = Cosine.get_cosine(tfidfBowQ, tfidfBowD1)
    # res2 = Cosine.get_cosine(tfidfBowQ, tfidfBowD2)
    # print('\n=====================Coosine==================')
    # print(res1)
    return res1

def greeting(message):
    for m in message:
        if m in GREETING_INPUTS:
            res = Respon.objects.filter(kategori=1).first()
            return res.text

def penutup(message):
    for m in message:
        if m in PENUTUP_INPUTS:
            res = Respon.objects.filter(kategori=10).first()
            return res.text

def kategoriCheck(token):
    res = KategoriPesan.objects.filter(nama=token).first()
    return res

def check_all_messages(message):
    highest_prob_list = {}
    result_cosine_list = {}
    check = {}

    def response(bot_response, list_of_words, single_response=False, required_words=[]):
        nonlocal highest_prob_list
        # print(message, list_of_words, single_response, required_words)
        highest_prob_list[bot_response] = message_probability(message, list_of_words, single_response, required_words)
        check[bot_response] = highest_prob_list[bot_response]
        # print('\n')
        # print(highest_prob_list[bot_response])

    def process(bot_response, d1, d2):
        nonlocal result_cosine_list
        result_cosine_list[bot_response] = TfidfCosSim(message, d1, d2)

    # data = getData()
    data = getDataKategori()
    # print(data)

    if greeting(message):
        res = greeting(message)
        return res
    elif penutup(message):
        res = penutup(message)
        return res
    else:
        kategoriKey = ''
        for r in message:
            if kategoriCheck(r) != None:
                kategoriKey = kategoriCheck(r)
        responData = getData(kategoriKey)
        # print(responData)

        single_resp = False
        required_w = []
        token = []

        for respon in responData:
            # print(respon['text'])
            tokenRespon = Preprocessing(respon['text']).run()
            # tokenRespon = respon['token']
            textRespon = respon['text']
            # print(tokenRespon)
            response(textRespon, tokenRespon, single_resp, respon['token'])

        
        # for r in data:
        #     single_resp = False
        #     required_w = []
        #     token = []
        #     # token = Preprocessing(r.text).run()

        #     if r.kategori_id == 1:
        #         single_resp = True
        #     # elif r.kategori_id == 2:
        #     #     required_w = ['buka', 'open']
        #     # elif r.kategori_id == 3:
        #     #     required_w = ['tutup', 'close']
        #     # elif r.kategori_id == 4:
        #     #     required_w = ['tiket', 'ticket', 'entrance']
        #     # elif r.kategori_id == 5:
        #     #     required_w = ['weekend']
        #     # elif r.kategori_id == 6:
        #     #     required_w = ['lokasi', 'location']
        #     # elif r.kategori_id == 7:
        #     #     required_w = ['fasilitas', 'facility']
        #     # elif r.kategori_id == 8:
        #     #     required_w = ['akses', 'access']
        #     # elif r.kategori_id == 9:
        #     #     required_w = ['parkir', 'parking']
        #     # /////////////////////////////////////////////////////////
        #     # elif r.kategori_id == 2:
        #     #     required_w = token
        #     # elif r.kategori_id == 3:
        #     #     required_w = token
        #     # elif r.kategori_id == 4:
        #     #     required_w = token
        #     # elif r.kategori_id == 5:
        #     #     required_w = token
        #     # elif r.kategori_id == 6:
        #     #     required_w = token
        #     # elif r.kategori_id == 7:
        #     #     required_w = token
        #     # elif r.kategori_id == 8:
        #     #     required_w = token
        #     # elif r.kategori_id == 9:
        #     #     required_w = token

        #     if r.token == None:
        #         token = Preprocessing(r.text).run()

        #     print(r.text, r.token, single_resp, required_w)
        #     response(r.text, r.token, single_resp, token)

        bot1 = max(highest_prob_list, key=highest_prob_list.get)
        highest_prob_list.pop(bot1)
        bot2 = max(highest_prob_list, key=highest_prob_list.get)
        highest_prob_list.pop(bot2)

        print('\n=====================highest_prob_list==================')
        print(highest_prob_list)
        print(bot1)
        print(bot2)

        # process(bot1, Preprocessing(bot1).run(), Preprocessing(bot2).run())
        # process(bot2, Preprocessing(bot2).run(), Preprocessing(bot1).run())

        # print(highest_prob_list)
        # print(result_cosine_list)
        best_match = max(result_cosine_list, key=result_cosine_list.get)

        # print(bot1)
        # print(bot2)

        if result_cosine_list[best_match] == 0.0:
            # if check[bot1] < 1:
            #     return unknown()
            # else:
            #     return bot1
            return 0
        else:
            return best_match

def unknown():
    return 'tidak ditemukan atau format salah'

def get_response(user_input):
    qToken = Preprocessing(user_input).run()
    print("\n=======================qToken====================")
    # print(' '.join(qToken))
    print(qToken)
    response = check_all_messages(qToken)
    # print("\n=======================Response====================")
    # print(response)
    if response == 'tidak ditemukan':
        pesan = user_input.translate(str.maketrans('','', string.punctuation))
        unknown_res = '"'+pesan+'"'+' '+response
        return unknown_res
    else:
        return response
