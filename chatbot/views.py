from django.shortcuts import render, redirect

from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.hashers import make_password
from django.contrib.auth import update_session_auth_hash
from django.utils.crypto import get_random_string
from django.core.paginator import Paginator
from django.core import serializers
from django.db.models import Q

from django.contrib import messages
from django.http import JsonResponse
import operator
import functools

from chatbot.models import *
from chatbot.preprocessing import *
# from chatbot.Get_Respon import *
from pprint import pprint
from .forms import *
from chatbot.getResponNew import get_response

from nltk.probability import FreqDist

# Create your views here.
def index(request):
    tempat = Tempat.objects.all()

    p = Paginator(tempat, 3)
    page = request.GET.get('page')
    objek = p.get_page(page)

    konteks = {
        'title': 'landing',
        'subtitle': '',
        'active': 'landing',
        'wisata': objek,
    }
    return render(request, 'user/index.html', konteks)

def register(request):
    form = CreateUserForm()

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(
                request, 'Akun ' + user + ' Berhasil dibuat, silakan login untuk melanjutkan', extra_tags='success')
            return redirect('login')

    konteks = {
        'title': 'register',
        'subtitle': '',
        'active': 'register',
        'form': form,
    }
    return render(request, 'registration/register.html', konteks)

def login(request):

    form = LoginUserForm()
    # form = LoginUserForm()

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            auth_login(request, user)
            if user.is_staff:
                return redirect('/admin')
            else:
                return redirect('dashboard')
        else:
            messages.info(request, 'Username atau password salah',
                          extra_tags='danger')

    konteks = {
        'title': 'login',
        'subtitle': '',
        'active': 'login',
        'form': form,
    }
    return render(request, 'registration/login.html', konteks)

def logout(request):
    auth_logout(request)
    return redirect('landing')

def guest(request):

    rand = get_random_string(length=3)
    username = 'user'+rand
    user = User.objects.create_user(
            username = username,
            email = username+'@email.com',
            password = make_password('1234678')
        )

    auth_login(request, user)

    return redirect('chat')

@login_required
def dashboard(request):
    tempat = Tempat.objects.all()

    p = Paginator(tempat, 3)
    page = request.GET.get('page')
    objek = p.get_page(page)

    konteks = {
        'title': 'dashboard',
        'subtitle': '',
        'active': 'index',
        'wisata': objek,
    }
    return render(request, 'user/dashboard.html', konteks)

@login_required
def wisata(request):
    tempat = Tempat.objects.all()
    konteks = {
        'title': 'wisata',
        'subtitle': '',
        'active': 'wisata',
        'wisata': tempat,
    }
    return render(request, 'user/wisata.html', konteks)

@login_required
def chat(request):
    # tempat = Tempat.objects.all()
    # template = Respon.objects.filter(kategori=11).values('text');

    konteks = {
        'title': 'chat',
        'subtitle': '',
        'active': 'chat',
        # 'template': list(template),
    }
    return render(request, 'user/chat.html', konteks)

# @login_required
def kirimPesan(request):    #noted
    if request.is_ajax and request.method == "POST":

        response = 'Success'
        nama = request.POST['nama']
        text = request.POST['text']
        lang = request.POST['lang']

        print(text, lang)

        respon = get_response(text, lang)

        # res = serializers.serialize('json', dtext, fields=('nama', ))
        # res = serializers.serialize('json', respon)
        # res = respon

        return JsonResponse({
            "response": response,
            'nama': nama,
            'pesan': text,
            'respon': respon,
        })

    return JsonResponse({}, status = 400)

@login_required
def templateChat(request):
    if request.is_ajax and request.method == "GET":
        template = Respon.objects.filter(kategori=11).values('text');
        data = {
            "response": 'Success',
            "pesan": list(template),
        }
        return JsonResponse(data)

    return JsonResponse({}, status = 400)

# @login_required
def getResponse(request):
    if request.is_ajax and request.method == "GET":

        respon = Respon.objects.filter(kategori=1).first();

        data = {
            "response": 'Success',
            "pesan": respon.text,
        }

        return JsonResponse(data)

    return JsonResponse({}, status = 400)

@login_required
def profile(request):
    if request.method == 'POST' and request.user.is_authenticated:
        user_form = ProfileForm(request.POST, instance=request.user)
        # user_form = UpdateUserForm(request.POST, instance=request.user)
        password = request.POST['password']

        if user_form.is_valid():
            user = user_form.save(commit=False)
            user.password = make_password(password)
            user.save()

            # auth_user = authenticate(request, username=user.username, password=user.password)

            # if user is not None:
            #     auth_login(request, auth_user)

            update_session_auth_hash(request, request.user)

            messages.success(request, 'Profil berhasil diubah', extra_tags='success')
            return redirect('profile')
    else:
        form = ProfileForm(instance=request.user)

    konteks = {
        'title': 'profile',
        'subtitle': '',
        'active': 'profil',
        'profileForm': form,
    }
    return render(request, 'user/profil.html', konteks)
