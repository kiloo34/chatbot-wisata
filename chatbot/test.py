# import nltk
# import numpy as np
# import random
# import string 
# from langdetect import detect
# from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
# from sklearn.feature_extraction.text import TfidfVectorizer
# #Cosing Similarity
# from sklearn.metrics.pairwise import cosine_similarity

# f=open('chatbot\chatbot.txt','r',errors = 'ignore')
# raw=f.read()
# raw=raw.lower()# converts to lowercase
# nltk.download('punkt') # first-time use only
# nltk.download('wordnet') # first-time use only
# sent_tokens = nltk.sent_tokenize(raw)# converts to list of sentences 
# word_tokens = nltk.word_tokenize(raw)# converts to list of words

# ## see the output
# sent_tokens[:2]

# word_tokens[:2]

# lemmer = nltk.stem.WordNetLemmatizer()

# def LemTokens(tokens):
#      '''LemTokens which will take as input the tokens and return normalized tokens.'''
#      return [lemmer.lemmatize(token) for token in tokens]
# remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)

# def LemNormalize(text):
#     return LemTokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))

# def response(user_response):
#     robo_response=''
#     sent_tokens.append(user_response)
#     if detect(user_response) == 'id':
#         TfidfVec = TfidfVectorizer(tokenizer=LemNormalize, stop_words='indonesian')
#     else:
#         TfidfVec = TfidfVectorizer(tokenizer=LemNormalize, stop_words='english')
#     tfidf = TfidfVec.fit_transform(sent_tokens)
#     vals = cosine_similarity(tfidf[-1], tfidf)
#     idx=vals.argsort()[0][-2]
#     flat = vals.flatten()
#     flat.sort()
#     req_tfidf = flat[-2]
#     if(req_tfidf==0):
#         robo_response=robo_response+"I am sorry! I don't understand you"
#         return robo_response
#     else:
#         robo_response = robo_response+sent_tokens[idx]
#         return robo_response

# GREETING_INPUTS = ("hello", "hi", "greetings", "sup", "what's up","hey",)
# GREETING_RESPONSES = ["hi", "hey", "*nods*", "hi there", "hello", "I am glad! You are talking to me"]

# def greeting(sentence):
#      for word in sentence.split():
#           if word.lower() in GREETING_INPUTS:
#                return random.choice(GREETING_RESPONSES)

# response('harga tiket pantai tampora berapa?')

import numpy as np
# import pandas as pd
import string
# import re
import nltk
import math
# import copy

from nltk.stem.porter import *
from langdetect import detect
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
# from collections import Counter

# from sklearn.metrics.pairwise import cosine_similarity, linear_kernel
# from sklearn.feature_extraction.text import TfidfVectorizer

# import chatbot.preprocessing
# from chatbot.models import Respon

# def getData():
#     data = Respon.objects.all().exclude(kategori__nama='template')
#     return data

# data = getData()

# vec = TfidfVectorizer(lowercase=True)
# X = vec.fit_transform(list(data))
# question = X.toarray()

# while True:
#     text = input('Me: ').lower()
#     if text in exit_list:
#         print('Bot: Chat with you later, stay safe!!')
#         break
#     else:
        # text_vec = vec.transform([text])
        # text_vec = text_vec.toarray()

#         text_vec = text_vec[0].reshape(1, -1) 
#         similarity = cosine_similarity(text_vec, question)
#         answer_index = np.argmax(similarity[0])
#         answer = df[1].iloc[answer_index]
#         print('Bot: ', answer)

q = "Telempong tutup jam berapa ya?"
d2 = "pantai tampora tutup jam 16.00"
d1 = "air terjun telempong tutup jam 16.00" 
d3 = "Pasir Putih tutup jam 16.00" 
d4 = "Wisata embara rengganis tutup jam 16.00" 

tq = q.translate(str.maketrans('','', string.punctuation))
td1 = d1.translate(str.maketrans('','', string.punctuation))
td2 = d2.translate(str.maketrans('','', string.punctuation))
td3 = d3.translate(str.maketrans('','', string.punctuation))
td4 = d4.translate(str.maketrans('','', string.punctuation))

print("\n================= punctuation ==============")
print(tq)
print(td1)
print(td2)
print(td3)
print(td4)

cq = tq.casefold()
cd1 = td1.casefold()
cd2 = td2.casefold()
cd3 = td3.casefold()
cd4 = td4.casefold()

print("\n================= casefold ==============")
print(cq)
print(cd1)
print(cd2)
print(cd3)
print(cd4)

crmq = re.sub(r"\d+", "", cq)
crmd1 = re.sub(r"\d+", "", cd1)
crmd2 = re.sub(r"\d+", "", cd2)
crmd3 = re.sub(r"\d+", "", cd3)
crmd4 = re.sub(r"\d+", "", cd4)

print("\n================= remove number ==============")
print(crmq)
print(crmd1)
print(crmd2)
print(crmd3)
print(crmd4)

tokq = nltk.word_tokenize(crmq)
tokd1 = nltk.word_tokenize(crmd1)
tokd2 = nltk.word_tokenize(crmd2)
tokd3 = nltk.word_tokenize(crmd3)
tokd4 = nltk.word_tokenize(crmd4)

print("\n================= tokenize ==============")
print(tokq)
print(tokd1)
print(tokd2)
print(tokd3)
print(tokd4)

def stemming(text, target):
    if detect(text) == 'id':
        ps = StemmerFactory()
        stemmer = ps.create_stemmer()
        res = [None] * len(target)
        i = 0
        for w in target:
            res[i] = stemmer.stem(w)
            i+=1
        return res
    else:
        ps = PorterStemmer()
        res = [None] * len(target)
        i = 0
        for w in target:
            res[i] = ps.stem(w)
            i+=1
        return res

sq = stemming(q, tokq)
sd1 = stemming(d1, tokd1)
sd2 = stemming(d2, tokd2)
sd3 = stemming(d3, tokd3)
sd4 = stemming(d4, tokd4)

print("\n================= stemming ==============")
print(sq)
print(sd1)
print(sd2)
print(sd3)
print(sd4)

def filtering(text, target):
    if detect(text) == 'id':
        listStopword =  set(stopwords.words('indonesian'))
    else:
        listStopword =  set(stopwords.words('english'))

    res = []
    for t in target:
        if t not in listStopword:
            res.append(t)
    return res

fq = filtering(q, sq)
fd1 = filtering(d1, sd1)
fd2 = filtering(d2, sd2)
fd3 = filtering(d3, sd3)
fd4 = filtering(d4, sd4)

print("\n================= filtering ==============")
print(fq)
print(fd1)
print(fd2)
print(fd3)
print(fd4)
        
# wordSet = set(fq).union(set(fd1)).union(set(fd2)).union(set(fd3)).union(set(fd4))
# def computeWordSet(wordSet, filteringToken):
#     data = {}
#     return data | filteringToken
    
# wordSet = {}
# wordSet = computeWordSet(wordSet, set(fq))
# wordSet = computeWordSet(wordSet, set(fd1))
# wordSet = computeWordSet(wordSet, set(fd2))
# wordSet = computeWordSet(wordSet, set(fd3))
# wordSet = computeWordSet(wordSet, set(fd4))

def Union(lst1,lst2):
    final_list = list(set().union(lst1,lst2))
    return final_list

ppList = [fq,fd1,fd2,fd3,fd4]
print("\n=================Count After Preprocessing==============")
print(len(ppList))
# print(ppList)
wordSet = [] 
wordSet = Union(wordSet,fq)
wordSet = Union(wordSet,fd1)
wordSet = Union(wordSet,fd2)
# wordSet = Union(wordSet,fd3)
# wordSet = Union(wordSet,fd4)

print(wordSet)

wordDictq = dict.fromkeys(wordSet, 0) 
wordDictA = dict.fromkeys(wordSet, 0) 
wordDictB = dict.fromkeys(wordSet, 0) 
# wordDictC = dict.fromkeys(wordSet, 0) 
# wordDictD = dict.fromkeys(wordSet, 0) 

print("\n=================worddict==============")
print(wordDictq)
print(wordDictA)
print(wordDictB)
# print(wordDictC)
# print(wordDictD)

def wordDict(token, wordDict):
    for word in token:
        if word not in wordDict:
            pass
        else:
            wordDict[word]+=1    
    return wordDict

freqQ = wordDict(fq, wordDictq)
freqd1 = wordDict(fd1, wordDictA)
freqd2 = wordDict(fd2, wordDictB)
# freqd3 = wordDict(fd3, wordDictC)
# freqd4 = wordDict(fd4, wordDictD)

print("\n=================wordFreq==============")
print(freqQ)
print(fq)
print(freqd1)
print(fd1)
print(freqd2)
print(fd2)
# print(freqd3)
# print(fd3)
# print(freqd4)
# print(fd4)
# print(wordDictq)
# print(wordDictA)
# print(wordDictB)
# print(wordDictC)
# print(wordDictD)

# contoh = ([wordDictq, wordDictA, wordDictB])
# print(len(contoh))

def computeTF(wordDict, bow):
    tfDict = {}
    bowCount = len(bow)
    # print(bowCount)
    for word, count in wordDict.items():
        tfDict[word] = count/float(bowCount)

    return tfDict

tfBowq = computeTF(freqQ, fq)
tfBowA = computeTF(freqd1, fd1)
tfBowB = computeTF(freqd2, fd2)
# tfBowC = computeTF(freqd3, fd3)
# tfBowD = computeTF(freqd4, fd4)

print("\n=================TF==============")
print(tfBowq)
print(tfBowA)
print(tfBowB)
# print(tfBowC)
# print(tfBowD)

def computeIDF(docList):
    idfDict = {}
    N = len(docList)
    print(N)
    idfDict = dict.fromkeys(docList[0].keys(), 0)
    for doc in docList:
        for word, val in doc.items():
            if val > 0:
                idfDict[word] += 1
    
    for word, val in idfDict.items():
        idfDict[word] = math.log10(N / float(val)) if float(val) != 0 else 0
        
    return idfDict    

# idfs = computeIDF([wordDictq, wordDictA, wordDictB, wordDictC, wordDictD])
idfs = computeIDF([wordDictq, wordDictA, wordDictB])
print("\n=================idfs==============")
print(idfs)

def computeTFIDF(tfBow, idfs):
    tfidf = {}
    for word, val in tfBow.items():
        # print(word)
        tfidf[word] = val*idfs[word]
    return tfidf

tfidfBowq = computeTFIDF(tfBowq, idfs)
tfidfBowA = computeTFIDF(tfBowA, idfs)
tfidfBowB = computeTFIDF(tfBowB, idfs)
# tfidfBowC = computeTFIDF(tfBowC, idfs)
# tfidfBowD = computeTFIDF(tfBowD, idfs)

print("\n=================TFIDFS==============")
print(tfidfBowq)
print(tfidfBowA)
print(tfidfBowB)
# print(tfidfBowC)
# print(tfidfBowD)

def get_cosine(vec1, vec2):
     intersection = set(vec1.keys()) & set(vec2.keys())
     numerator = sum([vec1[x] * vec2[x] for x in intersection])

     sum1 = sum([vec1[x]**2 for x in vec1.keys()])
     sum2 = sum([vec2[x]**2 for x in vec2.keys()])
     denominator = math.sqrt(sum1) * math.sqrt(sum2)

     if not denominator:
        return 0.0
     else:
        return float(numerator) / denominator

res1 = get_cosine(tfidfBowq, tfidfBowA)
res2 = get_cosine(tfidfBowq, tfidfBowB)
# res3 = get_cosine(tfidfBowq, tfidfBowC)
# res4 = get_cosine(tfidfBowq, tfidfBowD)

print("\n=================Cosine Similarity==============")
print(res1)
print(res2)
# print(res3)
# print(res4)

# text = [
#     'Tiket masuk pantai tampora 10.000 per orang',
#     'Tiket masuk pantai pasir putih 10.000 per orang',
#     'Pantai tampora buka setiap hari mulai pukul 08.00',
#     'Pantai pasir putih buka setiap hari mulai pukul 08.00',
#     'Pantai tampora tutup pukul 16.00',
#     'Pantai pasir putih tutup pukul 16.00',
#     'Pantai tampora tetap buka diwaktu weekend',
#     'Pantai pasir putih tetap buka diwaktu weekend',
# ]

# data = TfidfVectorizer().fit_transform(text)

# print("\n=================Coba Vectorizer==============")
# print(data[0:2])

# cosine_similarities = linear_kernel(data[0:1], data).flatten()

# print("\n=================Coba cosine_Sim==============")
# print(cosine_similarities)

# related_docs_indices = cosine_similarities.argsort()[:-5:-1]

# print("\n=================Coba Related==============")
# print(related_docs_indices)
# print("\n=================Coba Res==============")
# print(data.data[0])

# def cosine_sim(vec1, vec2):
#     vec1 = list(vec1.values())
#     vec2 = list(vec2.values())
#     dot_prod = 0
#     for i, v in enumerate(vec1):
#         dot_prod += v * vec2[i]
#     mag_1 = math.sqrt(sum([x**2 for x in vec1]))
#     mag_2 = math.sqrt(sum([x**2 for x in vec2]))
#     return dot_prod / (mag_1 * mag_2)

# print("\n=================CosineSim==============")
# print(cosine_sim(tfidfBowq, tfidfBowA))
# print(cosine_sim(tfidfBowA, tfidfBowB))

# query_vec = copy.copy(wordSet)
# tokens = re.findall(r'\w+', q.lower())
# token_counts = Counter(tokens)

# print("\n=================token count==============")
# print(tokens)
# print(token_counts)

# tokenized_documents = wordSet

# for key, value in token_counts.items():
#     docs_containing_key = 0
#     for _doc in tokenized_documents:
#         if key in _doc:
#             docs_containing_key += 1
#     if docs_containing_key == 0:
#         continue
#     tf = value / len(tokens)
#     idf = len(tokenized_documents) / docs_containing_key
#     query_vec[key] = tf * idf