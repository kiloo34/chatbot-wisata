import numpy as np
import string
import nltk
import math

from nltk.stem.porter import *
from langdetect import detect
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords

class Cosine:

    def Union(lst1,lst2):
        final_list = list(set().union(lst1,lst2))
        return final_list
    
    def wordDict(wordSet):
        return dict.fromkeys(wordSet, 0) 
    
    def freqQ(token, wordDict):
        for word in token:
            if word not in wordDict:
                pass
            else:
                wordDict[word]+=1    
        return wordDict

    def freq(index, token, wordDict):
        for word in token[index]:
            if word not in wordDict[index]:
                pass
            else:
                wordDict[index][word]+=1    
        return wordDict

    # Df Session
    def computeTFQ(wordDict, bow):
        tfDict = {}
        bowCount = len(bow)
        for word, count in wordDict.items():
            tfDict[word] = count/float(bowCount)
        return tfDict
    
    def computeTF(index, wordDict, bow):
        tfDict = {}
        bowCount = len(bow[index])
        # print(bowCount)
        for word, count in wordDict[index].items():
            tfDict[word] = count/float(bowCount)
        return tfDict

    # IDF Session
    def computeIDF(docList):
        idfDict = {}
        N = len(docList)
        idfDict = dict.fromkeys(docList[0].keys(), 0)
        for doc in docList:
            for word, val in doc.items():
                if val > 0:
                    idfDict[word] += 1
        
        for word, val in idfDict.items():
            idfDict[word] = math.log10(N / float(val)) if float(val) != 0 else 0
            
        return idfDict
    
    def computeTFIDF(tfBow, idfs):
        tfidf = {}
        for word, val in tfBow.items():
            tfidf[word] = val*idfs[word]
        return tfidf
    
    def get_cosine(vec1, vec2):
        intersection = set(vec1.keys()) & set(vec2.keys())
        numerator = sum([vec1[x] * vec2[x] for x in intersection])

        sum1 = sum([vec1[x]**2 for x in vec1.keys()])
        sum2 = sum([vec2[x]**2 for x in vec2.keys()])
        denominator = math.sqrt(sum1) * math.sqrt(sum2)

        if not denominator:
            return 0.0
        else:
            return float(numerator) / denominator