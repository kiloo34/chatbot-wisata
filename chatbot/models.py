from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class KategoriPesan(models.Model):
    nama = models.CharField('Kategori Pesan', max_length=50)
    list = models.JSONField(null=True, blank=True)
    en_nama = models.CharField(max_length=50)

    def __str__(self) -> str:
        return self.nama

    class Meta:
        db_table = 'chatbot_kategoripesan'
        ordering = ('nama', )
        # fields = ['nama']

class Pesan(models.Model):
    text = models.TextField()
    kategori = models.ForeignKey(KategoriPesan, on_delete=models.CASCADE, null=True)
    token = models.JSONField(null=True, blank=True)

    def __str__(self) -> str:
        return self.text
    class Meta:
        db_table = 'chatbot_pesan'
        ordering = ('text', )
        # fields = ['text', 'kategori']


class TipeHarga(models.Model):
    nama = models.CharField(max_length=50)

    def __str__(self) -> str:
        return self.nama

    class Meta:
        db_table = 'chatbot_tipeharga'
        ordering = ('nama',)
        # fields = ['nama']

class Tempat(models.Model):
    nama = models.CharField(max_length=50)
    alamat = models.TextField()
    desa = models.CharField(max_length=50)
    kecamatan = models.CharField(max_length=50)
    buka = models.CharField(max_length=10)
    tutup = models.CharField(max_length=10)
    foto = models.ImageField(upload_to='images/tempat/', null=True, blank=True)
    deskripsi = models.TextField(null=True, blank=True)

    def __str__(self) -> str:
        return self.nama

    class Meta:
        db_table = 'chatbot_tempat'
        ordering = ('nama',)
        # fields = ['nama', 'alamat', 'desa', 'kecamatan', 'buka', 'tutup']

class Respon(models.Model):
    text = models.TextField()
    kategori = models.ForeignKey(KategoriPesan, on_delete=models.CASCADE, null=True)
    tempat = models.ForeignKey(Tempat, on_delete=models.CASCADE, null=True)
    token = models.JSONField(null=True, blank=True)
    bahasa = models.CharField(max_length=50)
    
    def __str__(self) -> str:
        return self.text

    class Meta:
        db_table = 'chatbot_respon'
        ordering = ('id',)        
        # fields = ['text', 'kategori', 'tempat']

class Harga(models.Model):
    harga = models.CharField(max_length=15)
    tipe = models.ForeignKey(TipeHarga, on_delete=models.CASCADE, null=True)
    tempat = models.ForeignKey(Tempat, on_delete=models.CASCADE, null=True)

    def __str__(self) -> str:
        return self.harga
    
    class Meta:
        db_table = 'chatbot_harga'
        ordering = ('tempat',)
        # fields = ['harga', 'tipe', 'tempat']