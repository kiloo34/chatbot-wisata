from cgitb import text
from dataclasses import dataclass
import re
import copy
import math

from chatbot.preprocessing import *
from chatbot.models import KategoriPesan, Respon, Tempat
from collections import OrderedDict
from collections import Counter

GREETING_INPUTS = ("hallo", "halo", "hai", "selamat", "pagi","siang","sore","malam","hello","helo","hei","morning","evening","night","afternoon")
PENUTUP_INPUTS = ("terima", "terimakasih", "thank")

def greeting(message, lang):
    for m in message:
        if m in GREETING_INPUTS:
            res = Respon.objects.filter(kategori=1).filter(bahasa=lang).first()
            return res.text

def penutup(message, lang):
    for m in message:
        if m in PENUTUP_INPUTS:
            res = Respon.objects.filter(kategori=10).filter(bahasa=lang).first()
            return res.text

def getData(kategori, bahasa):
    return Respon.objects.filter(kategori__nama__contains=kategori).filter(bahasa=bahasa).values()

def getDataKategori():
    return KategoriPesan.objects.all()

def kategoriCheck(token, bahasa):
    res = ''
    print(token)
    if bahasa == 'id':
        res = KategoriPesan.objects.filter(nama=token).first()
    else:
        res = KategoriPesan.objects.filter(en_nama=token).first()
    return res

def detectLang(string):
    return 'en' if detect(string) == 'en' else 'id'

def cosine_sim(vec1, vec2):
    vec1 = list(vec1.values())
    vec2 = list(vec2.values())
    dot_prod = 0
    for i, v in enumerate(vec1):
        dot_prod += v * vec2[i]
    mag_1 = math.sqrt(sum([x**2 for x in vec1]))
    mag_2 = math.sqrt(sum([x**2 for x in vec2]))
    # print("\n=======================dot_prod====================")
    # print(dot_prod)
    # print("=======================dot_prod====================\n")
    # print("\n=======================mag_1====================")
    # print(mag_1)
    # print("=======================mag_1====================\n")
    # print("\n=======================mag_2====================")
    # print(mag_2)
    # print("=======================mag_2====================\n")
    print('dot_prod = ', dot_prod, ' ,mag_1 = ', mag_1, ' ,mag_2 = ',mag_2, ' Rumus dot_prod / (mag_1*mag_2)')
    return dot_prod / (mag_1 * mag_2) if dot_prod != 0 else 0

def process(pesan, kategoriKey, lang):
    responData = getData(kategoriKey, lang)
    textRespon = []
    for respon in responData:
        textRespon.append(respon['text'])

    # tokenized_documents = [re.findall(r'\w+', d['text'].lower()) for d in responData]
    tokenized_documents = [Preprocessing(d['text'], lang).run() for d in responData]
    print("\n=======================tokenized_documents====================")
    print(tokenized_documents)
    lexicon = sorted(set(sum(tokenized_documents, [])))
    print("\n=======================dictionary====================")
    print(lexicon)
    vector_template = OrderedDict((token, 0) for token in lexicon)
    print("\n=======================vector_template====================")
    print(vector_template)

    doc_tfidf_vectors = []
    for doc_tokens in tokenized_documents:
        vec = copy.copy(vector_template)
        token_counts = Counter(doc_tokens)
        for key, value in token_counts.items():
            docs_containing_key = 0
            for _doc_tokens in tokenized_documents:
                if key in _doc_tokens:
                    docs_containing_key += 1
            tf = value / len(doc_tokens)
            if docs_containing_key:
                idf = len(tokenized_documents) / docs_containing_key
            else:
                idf = 0
            vec[key] = tf * idf
        doc_tfidf_vectors.append(vec)
    
    print("\n=======================doc_tfidf_vectors====================")
    print(doc_tfidf_vectors)

    doc_tfidf_vectors_pesan = textProses(pesan, vector_template, tokenized_documents, lang)

    # print("\t0\t1\t2\t3\t4\t5\t6\t7\t8\t9\t10\t11\t12\t13\t14")
    # for r, doc1 in enumerate(doc_tfidf_vectors):
    #     print(r, end='\t')
    #     for c, doc2 in enumerate(doc_tfidf_vectors):
    #         print(round(cosine_sim(doc1, doc2), 2), end='\t')
    # print(doc_tfidf_vectors)

    res = {}
    for r, doc in enumerate(doc_tfidf_vectors):
        res[textRespon[r]] = round(cosine_sim(doc_tfidf_vectors_pesan, doc_tfidf_vectors[r]), 2)
    
    print("\n=======================hasil====================")
    print(res)
    return res

def getAllWisata():
    text = ''
    data = Tempat.objects.all().values('nama')
    for nama in data:
        text = text + str(nama['nama']) + ', '
    return text

def check_all_messages(pesan, lang):
    kategoriKey = ''
    # lang = detectLang(pesan)
    lang = lang
    qToken = Preprocessing(pesan, lang).run()
    tempat = ''

    for r in qToken:
        if set(r) == set('weekend'):
            kategoriKey = kategoriCheck(r, lang)
            break
        elif kategoriCheck(r, lang) != None:
            kategoriKey = kategoriCheck(r, lang)
    print(kategoriKey)
    if greeting(qToken, lang):
        print(lang)
        # print('masuk if')
        res = greeting(qToken, lang)
        return pesan+' '+res
    elif penutup(qToken, lang):
        print(lang)
        # print('masuk elif 1')
        res = penutup(qToken, lang)
        return res
    elif kategoriKey == '':
        print('masuk elif 2')
        if lang == 'id':
            kategoriKey = kategoriCheck('umum', lang)
            tempat = getAllWisata()
        elif lang == 'en':
            kategoriKey = kategoriCheck('general', lang)
            tempat = getAllWisata()
        else:
            return unknown()
        res = process(pesan, kategoriKey, lang)
        best_match = max(res, key=res.get)
    else:
        print('masuk else')
        res = process(pesan, kategoriKey, lang)
        best_match = max(res, key=res.get)

    if res[best_match] < 0.2:
        return unknown()
    else:
        print(tempat != '')
        if tempat != '':
            print('masuk')
            best_match = best_match + ' ' + tempat
        return best_match
        
def textProses(pesan, vector_template, tokenized_documents, lang):
    pesan_vec = copy.copy(vector_template)
    tokens = Preprocessing(pesan, lang).run()
    token_counts = Counter(tokens)

    for key, value in token_counts.items():
        docs_containing_key = 0
        for _doc in tokenized_documents:
            if key in _doc:
                docs_containing_key += 1
        if docs_containing_key == 0:
            continue
        tf = value / len(tokens)
        idf = len(tokenized_documents) / docs_containing_key
        pesan_vec[key] = tf * idf

    print("\n=======================tf====================")
    print(tf)
    print("=======================tf====================\n")
    print("\n=======================idf====================")
    print(idf)
    print("=======================idf====================\n")
    print("\n=======================vector====================")
    print(pesan_vec)
    print("=======================vector====================\n")
    return pesan_vec

def unknown():
    # store to database
    text = 'respon tidak ditemukan, silakan ganti bahasa atau hubungi agen Customer Service  '
    link = '<a href="https://wa.me/6282339275078" target="_blank"> disini </a>'
    return str(text)+' '+link

def get_response(user_input, lang):
    response = check_all_messages(user_input, lang)
    # print("\n=======================Response====================")
    # print(response)
    if response == 'tidak ditemukan atau format salah':
        pesan = user_input.translate(str.maketrans('','', string.punctuation))
        unknown_res = '"'+pesan+'"'+' '+response
        return unknown_res
    else:
        return response