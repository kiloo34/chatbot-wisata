from django.contrib import admin
from django.db import reset_queries
from chatbot.models import Pesan, KategoriPesan, Respon, Tempat, TipeHarga, Harga

# Register your models here.
#admin.site.register(Pesan)
admin.site.register(KategoriPesan)
admin.site.register(Respon)
admin.site.register(Tempat)
#admin.site.register(TipeHarga)
#admin.site.register(Harga)
