from django.contrib import admin
from django.urls import path

from django.conf import settings
from django.conf.urls.static import static


from chatbot import views

# if settings.DEBUG:
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', views.index, name='landing'),
    
    # auth
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('guest/', views.guest, name='guest'),
    path('logout/', views.logout, name='logout'),

    path('dashboard/', views.dashboard, name='dashboard'),
    path('wisata/', views.wisata, name='wisata'),
    path('profile/', views.profile, name='profile'),
    
    path('chat/', views.chat, name='chat'), #return to view
    path('ajax/kirimPesan', views.kirimPesan, name='kirimPesan'),
    path('ajax/templateChat/', views.templateChat, name='templateChat'),
    path('ajax/getResponse/', views.getResponse, name='getResponSystem'),
]

if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)